﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xanax.Models;
using Xanax.ViewModels;

namespace Xanax.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserDetailsPage : ContentPage
    {
        public UserDetailsPage(User user)
        {
            InitializeComponent();
            BindingContext = new UserDetailViewModel(user);
        }
    }
}