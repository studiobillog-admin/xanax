﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xanax.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            usersList.ItemSelected += (s, e) => usersList.SelectedItem = null;
        }
    }
}