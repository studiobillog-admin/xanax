﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xanax.Models
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Avatar { get; set; }
        public string Name { get => FirstName + " " + LastName; } 
    }
}
