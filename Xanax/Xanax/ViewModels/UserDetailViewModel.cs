﻿using System;
using System.Collections.Generic;
using System.Text;
using Xanax.Models;

namespace Xanax.ViewModels
{
    class UserDetailViewModel
    {
        public User User { get; set; }
        public UserDetailViewModel(User user)
        {
            User = user;
        }

    }
}
