﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using Xanax.Models;
using Xanax.ViewModels.Base;
using Xanax.Views;

namespace Xanax.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get => _users;
            set => SetPropertyIfChanged(ref _users, value);
        }

        public User SelectedUser
        {
            set
            {
                if (value is null) return;
                SelectUser(value);   
            } 
        }
        private async void SelectUser(User user)
        {
            bool shouldDelete = await App.Current.MainPage.DisplayAlert("Musisz wybrać wariacie!", "Co robić?", "Usuń", "Wyświetl");

            if (shouldDelete)
            {
                Users.Remove(user);
            }
            else
            { 
                NavigateToUserDetails(user);
            }
        }
        private void NavigateToUserDetails(User user)
        {
            App.Current.MainPage.Navigation.PushAsync(new UserDetailsPage(user));
        }

        public HomeViewModel()
        {
            Users = new ObservableCollection<User>
            {
                new User { FirstName = "Daniel", LastName = "Załęski", Age = 22, Avatar = "https://picsum.photos/id/1/200/300" },
                new User { FirstName = "Kuba", LastName = "Kozar", Age = 21, Avatar = "https://picsum.photos/id/2/200/300" },
                new User { FirstName = "Wiktoria", LastName = "Wojtczak", Age = 21, Avatar = "https://picsum.photos/id/3/200/300"}
            };
        }

        private async void AddUser()
        {
            var firstName = await App.Current.MainPage.DisplayPromptAsync("Dodaj osobę", "Wpisz imię");

            var user = new User() {
                FirstName = firstName,
                LastName = "Nowy",
                Avatar = "https://picsum.photos/id/" + (Users.Count + 1) + "/200/300"
            };

            Users.Add(user);
        }

        public ICommand AddUserCommand => new Command(AddUser);

        //Praca domowa
        //1. Dodać button dodający nowych userów
        //2. Wyświetlić zapytanie o imię
        //3. Dodać usera do listy
    }
}
