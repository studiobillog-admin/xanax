﻿using System.Windows.Input;
using Xamarin.Forms;
using Xanax.Views;

namespace Xanax.ViewModels
{
    class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public ICommand LoginCommand => new Command(LoginExecute);

        private async void LoginExecute()
        {
            await App.Current.MainPage.DisplayAlert("Zalogowałeś się!", $"{Login} {Password}", "Nara");
            App.Current.MainPage = new NavigationPage(new HomePage());
        }

        //Praca domowa
        //Aplikacja z ekranem logowania:
        // -niech sprawdza czy passy są dobre (ustawić jakieś w kodzie),
        // -pop up gdy złe dane
        // -trzy guziki:
        //      1. Prowdzi do listy znajomych
        //      2. Do nowego page z trzema inputami (wielkość czcionki, kolor napisu, co to za napis) i na dole button "Zastosuj".
        //      3. Zaimplementować Carousel View z jakimiś obrazkami, swipe w lewo i prawo.

    }
}
